/**
 * Created by linxiaofen on 2017/5/26.
 */
'use strict';
describe('app', function () {
    beforeEach(module('app'));
    describe('indexCtrl', function () {
        var scope, ctrl;
        var valid_respond = readJSON('json/user.json');
        beforeEach(inject(function($controller, $rootScope){
            scope = $rootScope.$new();
            ctrl = $controller('indexCtrl',{$scope:scope});
        }));
        it('add 测试', inject(function () {
            expect(scope.add(2, 3)).toEqual(5);
        }));
        it('should create name william wood in unitTestCtrl',inject(function(){
            expect(scope.name).toEqual('william wood');
        }));
        it('GetUser should fetch users',inject(function($injector){
            var $httpBackend = $injector.get('$httpBackend');
            $httpBackend.when('GET','/auth.py').respond(valid_respond);
            scope.getUser();
            $httpBackend.flush();
            expect(scope.user.length).toBe(2);
        }))
    });
    describe("mainCtrl", function() {
        var bar,scope,ctrl;

        beforeEach(function() {
            bar = 1;
        });
        beforeEach(inject(function($controller,$rootScope){
            scope = $rootScope.$new();
            ctrl = $controller('mainCtrl',{$scope:scope});
        }));

        it("foo should be equal bar", function() {
            expect(scope.foo).toEqual(bar);
        });
    });
});