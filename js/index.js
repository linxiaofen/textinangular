/**
 * Created by linxiaofen on 2017/5/26.
 */
'use strict';
(function (angular) {
    angular.module('app', []).
    controller('indexCtrl', function ($scope, $http) {
        $scope.name = "william wood";
        $scope.getUser = function(){
            $http.get('/auth.py').then(function(response){
                $scope.user = response.data;
            })
        };
        $scope.add = function (a, b) {
            if(a&&b)
                return Number(a) + Number(b);
            return 0;
        }
    })
        .controller('mainCtrl',function($scope){
        $scope.foo = 1;
    });
})(window.angular);